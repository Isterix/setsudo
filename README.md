# Setsudo

## Introduction

Setsudo est un robot de modération en français pour votre serveur Discord.

## Commandes

### Utilisables par les modérateurs

* !kick - suivie d'un ping de la personne ciblée, elle vous permet de "kick" (d'exclure) quelqu'un du serveur.
* !ban - suivie d'un ping de la personne ciblée, cette commande vous permet de "ban" (de bannir) quelqu'un du serveur. Si le ping est suivi d'un nombre, le ban ne durera que pendant le nombre de jours indiqué (max 7).
* !unban - suivie d'un identifiant d'utilisateur, cette commande annule (prématurément) un ban effectif, ce qui veut dire qu'une personne débannie pourra à nouveau rejoindre votre serveur, si elle le souhaite.
* !mute - suivie d'un ping de la personne ciblée, elle fait en sorte de "mute" (de muter) quelqu'un d'écrire ou de parler dans votre serveur.
* !unmute - suivie d'un ping de la personne ciblée, cette commande retire le rôle "muté" d'un membre du serveur, afin qu'ils puissent à nouveau y écrire et y parler.
* !delmsg - suivie d'un ping de la personne ciblée, elle supprime tous les messages écrits par une personne dans le salon où la commande a été faite (limite d'environ 85 à 99 messages par commande).

### Utilisables par tout le monde

* !random - suivie d'un nombre, elle fait en sorte que Setsudo vous donne un nombre aléatoire, minimum 1, maximum celui donné dans la commande. Suivie de deux nombres, le chiffre minimum est le premier donné dans la commande.
* !avatar - suivie d'un ping, elle vous donne l'avatar d'un utilisateur à la taille maximale. Sans argument, elle donne votre avatar, toujours à la taille maximale.

## Comment faire pour faire tourner ce robot ?

Pour ce faire, vous aurez besoin de télécharger l'intégralité du code ainsi que [Node.js](https://nodejs.org/fr/).

Vous allez aussi besoin d'un corps pour votre robot. Ce code est le cerveau, mais vous lui donnez un corps ! Pour ça, rendez-vous sur [cette page](https://discord.com/developers/applications) et mettez un bot token dans le fichier .env.

Une fois tout ça fait, vous pouvez utiliser la console de commande là où le code est localisé dans votre appareil, puis faire la commande "node index.js".

N'hésitez pas à me contacter sur Discord dans le cas où vous rencontrez un souci: Istérix#1287

## Crédits

Ce robot dépend de [discord.js](https://discord.js.org) et de [dotenv](https://github.com/motdotla/dotenv#readme) pour fonctionner.

## License

This project is licensed under the GNU General Public License - see the LICENSE file for details
