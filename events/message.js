const kick = require('../commands/kick')
const ban = require('../commands/ban')
const unban = require('../commands/unban')
const mute = require('../commands/mute')
const unmute = require('../commands/unmute')
const delmsg = require('../commands/delmsg')
const random = require('../commands/random')
const avatar = require('../commands/avatar')
prefix = '!'

module.exports = (client, message) => {
	if (!message.guild) return
	if (message.member.hasPermission('BAN_MEMBERS')) {
		if (message.content.startsWith(`${prefix}kick`)) {
			return kick(message)
		}
		if (message.content.startsWith(`${prefix}ban`)) {
			return ban(message)
		}
		if (message.content.startsWith(`${prefix}unban`)) {
			return unban(message)
		}
		if (message.content.startsWith(`${prefix}mute`)) {
			return mute(message)
		}
		if (message.content.startsWith(`${prefix}unmute`)) {
			return unmute(message)
		}
		if (message.content.startsWith(`${prefix}delmsg`)) {
			return delmsg(message)
		}
	}
	if (message.content.startsWith(`${prefix}random`)) {
		return random(message)
	}
	if (message.content.startsWith(`${prefix}avatar`)) {
		return avatar(message)
	}
}
