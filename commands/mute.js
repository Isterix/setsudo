module.exports = message => {

	async function syncronize(message) { //Making the whole thing syncronized prevents issues to happen if the Muted role gets created too late

		const nom_du_role_muted = "Muté"

		const member = message.mentions.members.first() //Check for ping
		if (!member) {return message.reply(`vous devez mentionner l'utilisateur que vous voulez mute lorsque vous utilisez cette commande !`)}
		
		let existe = message.guild.roles.cache.find(x => x.name == "Muted") //Create muted role if it does not exist
		if (!existe) {
			await message.guild.roles.create({
				data: {
					name: nom_du_role_muted,
					color: "696969",
					hoist: false,
					position: 1,
					permissions: ['READ_MESSAGE_HISTORY'],
					mentionable: false
				},
			reason: "Automatiquement crée par Setsudo afin que la commande mute soit efficace."
			})
		}
		
		message.guild.roles.cache.forEach(role => {if (role.name == nom_du_role_muted) {role_variable = role}}) //Get the role variable

		message.guild.channels.cache.forEach(channel => { //Make it so muted people cannot talk in any channel
			if (channel.type != "category") {
				channel.updateOverwrite(channel.guild.roles.resolve(role_variable), {SEND_MESSAGES: false, SEND_TTS_MESSAGES: false, SPEAK: false}, "Automatiquement modifié par Setsudo.")
				.catch(error => console.log("(MUTE PERMISSIONS) An error has happened: " + String(error)))
			}
		})
		member.roles.add(role_variable)
		.catch(error => console.log("(MUTE ADDING ROLE TO USER) An error has happened: " + String(error)))
	}

	syncronize(message)
}
