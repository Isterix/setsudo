module.exports = message => {
	const args = message.content.slice(prefix.length).split(' ')
	if (args.length < 2) {return message.channel.send(`${message.author} Cette commande requiert au moins un argument !`)}
	
	if (args.length === 2) {
		try {maximum = Number(args[1])}
		catch {return message.channel.send(`${message.author} L'argument donné n'est pas un nombre !`)}
		return message.channel.send(Math.floor((Math.random() * maximum) + 1))
	}

	if (args.length > 2) {
		try {
			minimum = Number(args[1])
			maximum = Number(args[2])
		}
		catch {return message.channel.send(`${message.author} L'un des arguments donnés n'est pas un nombre !`)}
		return message.channel.send(Math.floor(Math.random() * (maximum - minimum + 1) + minimum))
	}

}
