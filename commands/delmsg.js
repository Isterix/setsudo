module.exports = message => {
	
	async function syncronize(message) { //Making the whole thing syncronized prevents issues to happen with the fetching of messages being too slow
		
		const member = message.mentions.members.first() //Check for ping
		if (!member) {return message.reply(`vous devez mentionner l'utilisateur afin de supprimer ses messages de ce salon !`)}

		const all_msgs = await message.channel.messages.fetch({limit: 99})
		.catch(error => console.log("(DELMSG FETCHING) An error has happened: " + String(error)))
		d = 0
		await all_msgs.forEach(to_delete => {
			if (to_delete.author.id == member.id) {
				to_delete.delete()
				.catch(error => console.log("(DELMSG DELETING) An error has happened: " + String(error)))
				d++
			}
		})
		await message.channel.send(`${d} messages sont en train d'être supprimés.`)

	}

	syncronize(message)
}
