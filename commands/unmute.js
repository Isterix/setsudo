module.exports = message => {

	const member = message.mentions.members.first()
	if (!member) {return message.reply(`vous devez mentionner l'utilisateur que vous voulez unmute lorsque vous utilisez cette commande !`)}

	message.guild.roles.cache.forEach(role => {if (role.name == "Muted") {role_variable = role}}) //Get the role variable

	if (member.roles.cache.has(role_variable.id)) {
		member.roles.remove(role_variable)
		message.channel.send(`${member} Vous avez été unmute !`)
	} else {
		message.reply(`la personne que vous voulez unmute n'est pas mute !`)
	}
	
}
