module.exports = message => {
	
	const args = message.content.slice(prefix.length).split(' ')

	if (args.length < 2) {return message.reply(`vous devez suivre cette commande par l'ID de l'utilisateur banni !`)}

	if (String(Number(args[1])) === "NaN") {return message.reply(`${args[1]} n'est pas un ID d'utilisateur valide !`)}

	message.guild.members.unban(args[1])
	.then(user => message.channel.send(`${user.tag} a été débanni par ${message.author} !`))
	.catch(error => {
		message.reply(`l'utilisateur à l'ID ${args[1]} n'a pas pu être débanni..`)
		console.log("(UNBAN PROCESS) An error has happened: " + String(error))
	})

}
