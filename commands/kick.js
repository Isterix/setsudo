module.exports = message => {

	const member = message.mentions.members.first()

	if (!member) {return message.reply(`vous devez mentionner l'utilisateur que vous voulez kick lorsque vous utilisez cette commande !`)}

	if (!member.kickable) {return message.reply(`je ne peux pas kick cet utilisateur...`)}

	return member
		.kick()
		.then(() => message.channel.send(`${member.user.tag} a été kick du serveur par ${message.author}.`))
		.catch(error => message.reply(`une erreur s'est produite...`))
    
}
