module.exports = message => {

	const member = message.mentions.members.first()
	const args = message.content.slice(prefix.length).split(' ')

	if (!member) {return message.reply(`vous devez mentionner l'utilisateur que vous voulez ban lorsque vous utilisez cette commande !`)}

	if (!member.kickable) {return message.reply(`je ne peux pas ban cet utilisateur...`)}

	if (args.length === 2) {
		return member
			.ban()
    		.then(() => message.channel.send(`${member.user.tag} a été ban du serveur par ${message.author}.\nPour l'unban, vous pouvez faire la commande "unban ${member.user.id}".`))
    		.catch(error => message.reply(`une erreur s'est produite...`))
	}

	if (args.length >= 3) {
		try {
			let days = Number(args[2])
			if (days > 7) {return message.reply(`dans le cas où vous voulez tempban, vous devez savoir qu'un tempban ne peut durer que 7 jours maximum !\nRéessayez en sachant cela.`)}
			return member
				.ban({days: days, reason: `(SETSUDO) Banni pour ${days} jours par ${message.author.tag} (${message.author}).`})
    			.then(() => message.channel.send(`${member.user.tag} a été tempban du serveur pour ${days} jour(s) par ${message.author}.\nPour l'unban, vous pouvez faire la commande "unban ${member.user.id}".`))
    			.catch(error => message.reply(`une erreur s'est produite...`))
		}
		catch {
			message.reply(`le nombre de jours spécifié après la mention est invalide !`)
		}
	}

}
